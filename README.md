# CommandQueue

Creates a menu on the scoreboard that lets you queue up items to have picked up automatically when you open a command cube

![Game screenshot](https://i.imgur.com/ma2kgok.png)

[Support me on Ko-fi](https://ko-fi.com/kuberoot)

## Features:

-   A full intuitive GUI accessible through a newly added tab on the scoreboard
-   Add items to the end of the queue and remove items from anywhere in the queue
-   Queues for the three item rarities, lunar and boss items, as well as void variants, available in separate tabs
    -   Lunar, boss and void item queues are disabled by default, can be enabled in config
-   Automatically selects items when you interact with a command cube, as long as it's available
    -   If none of the items at the end of the queues are available, the menu opens as normal
-   NEW: You can now make the queue loop, allowing you to farm the same sequence of items repeatedly

## Limitations

There is no controller support - the mod hasn't been tested with a controller and the UI is not in any way made to work with a controller.

The mod has not been comprehensively tested with the latest update, there might be issues or errors.

## Installation

Copy the `CommandQueue` folder to `Risk of Rain 2/BepInEx/plugins`

## Attribution

Looping icon based on [repeat from FontAwesome](https://fontawesome.com/icons/repeat?s=solid).

## Patch notes: 

- 1.4.0
    - Update for SotV patch 1.2.3
- 1.3.3
    - Fix item icons having the wrong scale when advancing to the next level, which caused items in the queue to start overlapping.
- 1.3.2
    - Add looping mode. If the button is enabled, when commandqueue chooses an item, the chosen item is added back at the end of the queue.
- 1.3.1
    - Plug my Ko-fi
- 1.3.0
    - Update the mod for the Survivors of the Void update
    - Add drag'n'drop for items in the queue
    - Add an option to remove a whole stack of items in the queue with right-click, enabled by default
    - Merge adjacent stacks of the same item in the queue, happens when an item between them is deleted
    - Stop queue items getting focus when clicked. Will prevent issues where game doesn't respond to input until you click.
- 1.1.0
    - Add expanded menu option (enabled by default)
    - Add support for Lunar and Boss item queues (disabled by default)
    - Change source of item rarities, should help compatibility in some cases
    - Minor improvements to UI (figured out pivots, things can be more dynamic)
- 1.0.0 - Initial release