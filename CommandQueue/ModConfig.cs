﻿using BepInEx.Configuration;
using System;
using System.Collections.Generic;
using RoR2;
using System.Linq;

namespace CommandQueue
{
    public static class ModConfig
    {
        private static ConfigFile config;
        
        internal static void InitConfig(ConfigFile _config)
        {
            config = _config; // No config available at the moment

            TomlTypeConverter.AddConverter(typeof(ItemTierSet), new TypeConverter
            {
                ConvertToObject = (str, type) => ItemTierSet.Deserialize(str),
                ConvertToString = (obj, type) => obj.ToString()
            });

            enabledTabs = config.Bind(new ConfigDefinition("General", "EnabledQueues"), new ItemTierSet { ItemTier.Tier1, ItemTier.Tier2, ItemTier.Tier3 }, new ConfigDescription($"Which item tiers should have queues?\nValid values: {string.Join(", ", Enum.GetNames(typeof(ItemTier)))}"));
            bigItemButtonContainer = config.Bind(new ConfigDefinition("General", "BigItemSelectionContainer"), true, new ConfigDescription("false: Default command button layout\ntrue: Increase the space for buttons, helps avoid overflow with modded items"));
            bigItemButtonScale = config.Bind(new ConfigDefinition("General", "BigItemSelectionScale"), 1f, new ConfigDescription("Scale applied to item buttons in the menu - decrease it if your buttons don't fit\nApplies only if BigItemSelectionContainer is true"));
            rightClickRemovesStack = config.Bind(new ConfigDefinition("General", "RightClickRemovesStack"), true, new ConfigDescription("Should right-clicking an item in the queue remove the whole stack?"));
        }

        public static ConfigEntry<ItemTierSet> enabledTabs;
        public static ConfigEntry<bool> bigItemButtonContainer;
        public static ConfigEntry<float> bigItemButtonScale;
        public static ConfigEntry<bool> rightClickRemovesStack;

        public class ItemTierSet : SortedSet<ItemTier>
        {
            public static string Serialize(ItemTierSet self)
            {
                return string.Join(", ", self.Select(x => x.ToString()));
            }
            public static ItemTierSet Deserialize(string src)
            {
                ItemTierSet self = new ItemTierSet();
                foreach(var entry in src.Split(',').Select(s => s.Trim()))
                {
                    if(Enum.TryParse(entry, out ItemTier result))
                    {
                        self.Add(result);
                    }
                    else if(int.TryParse(entry, out int index))
                    {
                        self.Add((ItemTier)index);
                    }
                }
                return self;
            }

            public override string ToString()
            {
                return Serialize(this);
            }
        }
    }
}
