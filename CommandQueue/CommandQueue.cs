﻿using BepInEx;
using RoR2;
using UnityEngine;
using RoR2.UI;
using System;
using System.Reflection;
using System.Collections;

/*
	string directoryName = Path.GetDirectoryName(ConfigFilePath);
	if (directoryName != null) Directory.CreateDirectory(directoryName);

	using (var writer = new StreamWriter(ConfigFilePath, false, Encoding.UTF8))

TODO:
Current tasks:
- Provide a way to setup (per-character) initial queues
 - Adapt ItemSetConfigWrapper from AutoItemPickup
 - Generate a short description in comments
 - Generate sections named after characters
 - Parse section contents for items
  - Decide on format
  ? \s* ItemName \s* \* Count \s* ,

Future tasks:
- Maybe create UI that shows other players' queue
 - Should players broadcast their queues even if server isn't aware?
- Toggle button that makes popped items get put back at the end of the queue, for repeatedly picking up the same items without setting them over again

*/

namespace CommandQueue
{
    [BepInDependency("com.bepis.r2api")]
    [BepInPlugin("com.kuberoot.commandqueue", "CommandQueue", "1.4.0")]
    [R2API.Utils.NetworkCompatibility(R2API.Utils.CompatibilityLevel.NoNeedForSync, R2API.Utils.VersionStrictness.DifferentModVersionsAreOk)]
    public class CommandQueue : BaseUnityPlugin
    {
        public static event Action PluginUnloaded;
        public static bool IsLoaded;

        private static GameObject commandUIPrefab;
        private static readonly FieldInfo commandCubePrefabField = typeof(RoR2.Artifacts.CommandArtifactManager).GetField("commandCubePrefab", BindingFlags.Static | BindingFlags.NonPublic);
        private static readonly FieldInfo PickupPickerController_options = typeof(PickupPickerController).GetField("options", BindingFlags.Instance | BindingFlags.NonPublic);

        public void Awake()
        {
            ModConfig.InitConfig(Config);
            ModConfig.enabledTabs.SettingChanged += (_, __) => FakeReload();
            ModConfig.bigItemButtonContainer.SettingChanged += (_, __) => FakeReload();
            ModConfig.bigItemButtonScale.SettingChanged += (_, __) => FakeReload();
        }

        private bool isFakeReloading = false;

        private void FakeReload()
        {
            if (isFakeReloading) return;
            isFakeReloading = true;
            IEnumerator doFakeReload()
            {
                yield return 0;
                OnDisable();
                yield return 0;
                OnEnable();
                isFakeReloading = false;
            }
            StartCoroutine(doFakeReload());
        }

        public void OnEnable()
        {
            commandUIPrefab = (commandCubePrefabField.GetValue(null) as GameObject)?.GetComponent<PickupPickerController>().panelPrefab;
            IsLoaded = true;

            On.RoR2.PickupPickerController.OnDisplayBegin += HandleCommandDisplayBegin;
            On.RoR2.UI.ScoreboardController.Awake += ScoreboardController_Awake;
            On.RoR2.Artifacts.CommandArtifactManager.Init += CommandArtifactManager_Init;
            QueueManager.Enable();
            
            foreach (var component in FindObjectsOfType<HUD>())
            {
                component.scoreboardPanel.AddComponent<UIManager>();
            }
        }

        public void OnDisable()
        {
            IsLoaded = false;
            PluginUnloaded?.Invoke();
            On.RoR2.PickupPickerController.OnDisplayBegin -= HandleCommandDisplayBegin;
            On.RoR2.UI.ScoreboardController.Awake -= ScoreboardController_Awake;
            On.RoR2.Artifacts.CommandArtifactManager.Init -= CommandArtifactManager_Init;
            QueueManager.Disable();
        }

        private void CommandArtifactManager_Init(On.RoR2.Artifacts.CommandArtifactManager.orig_Init orig)
        {
            orig();
            commandUIPrefab = (commandCubePrefabField.GetValue(null) as GameObject)?.GetComponent<PickupPickerController>().panelPrefab;
        }

        private void HandleCommandDisplayBegin(On.RoR2.PickupPickerController.orig_OnDisplayBegin orig, PickupPickerController self, NetworkUIPromptController networkUIPromptController, LocalUser localUser, CameraRigController cameraRigController)
        {
            if (self.panelPrefab == commandUIPrefab)
            {
                foreach(var (tier, index) in QueueManager.PeekAll())
                {
                    if (self.IsChoiceAvailable(index))
                    {
                        QueueManager.Pop(tier);
                        PickupPickerController.Option[] options = (PickupPickerController.Option[])PickupPickerController_options.GetValue(self);

                        for (int j = 0; j < options.Length; j++)
                        {
                            if(options[j].pickupIndex == index && options[j].available)
                            {
                                IEnumerator submitChoiceNextFrame()
                                {
                                    yield return 0;
                                    self.SubmitChoice(j);
                                }
                                self.StartCoroutine(submitChoiceNextFrame());
                                break;
                            }
                        }
                        
                        return;
                    }
                }
            }

            orig(self, networkUIPromptController, localUser, cameraRigController);
        }

        private void ScoreboardController_Awake(On.RoR2.UI.ScoreboardController.orig_Awake orig, ScoreboardController self)
        {
            self.gameObject.AddComponent<UIManager>();
            orig(self);
        }
    }
}